#!/usr/bin/env python3
import pop.hub


def start():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="heist")
    hub.heist.init.start()


if __name__ == "__main__":
    start()
