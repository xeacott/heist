import asyncio

import asynctest


def async_read_write(add, port, family=None):
    reader = asynctest.mock.Mock(asyncio.StreamReader)
    writer = asynctest.mock.Mock(asyncio.StreamWriter)

    reader.read.return_value = b"SSH-2.0-OpenSSH_8.5\r\n"
    return reader, writer
