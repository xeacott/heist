from unittest import mock

import pytest


@pytest.fixture(scope="function")
def hub(hub, tmp_path):
    for dyne in ["heist"]:
        hub.pop.sub.add(dyne_name=dyne)

    heist_conf = tmp_path / "heist.conf"
    with open(heist_conf, "w") as fp:
        pass

    with mock.patch("sys.argv", ["heist", "grains", f"--config={heist_conf}"]):
        hub.pop.config.load(
            ["heist"],
            cli="heist",
            parse_cli=True,
        )

    yield hub
