#!/usr/bin/python3
"""
    tests.unit.artifact.test_init
    ~~~~~~~~~~~~~~

    tests for artifact.init code
"""
import pathlib
import unittest.mock as mock
from unittest.mock import call

import pytest


@pytest.mark.asyncio
async def test_clean(mock_hub, hub):
    """
    test artifact.init.clean removes the correct directory
    """
    mock_hub.artifact.init.clean = hub.artifact.init.clean
    target_name = "test_target_name"
    run_dir = pathlib.Path("var") / "tmp" / "heist_root" / "abcd"
    mock_hub.heist.CONS = {}
    mock_hub.heist.CONS[target_name] = {}
    mock_hub.heist.CONS[target_name]["run_dir"] = run_dir

    await mock_hub.artifact.init.clean(target_name, "asyncssh")

    mock_hub.tunnel.asyncssh.cmd.call_args_list[0] == call(
        target_name, f"rm -rf {run_dir}"
    )
    mock_hub.tunnel.asyncssh.cmd.call_args_list[1] == call(
        target_name, f"rmdir {run_dir.parent}"
    )
