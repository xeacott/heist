#!/usr/bin/python3
"""
    tests.unit.heist.test_heist
    ~~~~~~~~~~~~~~

    tests for heist.init code
"""
import unittest.mock as mock

import pop.mods.pop.testing as testing
import pytest

import heist.heist.init


def test_start(hub, mock_hub):
    mock_hub.heist.init.start = hub.heist.init.start
    mock_hub.heist.init.start()
    mock_hub.pop.loop.start.assert_called_once()
    mock_hub.heist.init.run_remotes.assert_called_once()


def test_start_no_manager(hub, mock_hub):
    """
    Test the start method without defining a manager
    """
    mock_hub.heist.init.start = hub.heist.init.start
    mock_hub.SUBPARSER = ""
    assert not mock_hub.heist.init.start()


@pytest.mark.asyncio
async def test_run_remotes(mock_hub, hub, tempdir):
    mock_hub.heist.init.run_remotes = hub.heist.init.run_remotes
    mock_hub.OPT.heist = {"roster": mock.sentinel.roster, "manager": "grains"}

    await mock_hub.heist.init.run_remotes("grains", tempdir)

    mock_hub.roster.init.read.assert_called_once_with(
        None, roster_file="", roster_data=None
    )


@pytest.mark.asyncio
async def test_run_roster_false(mock_hub, hub, tempdir):
    """
    test heist.heist.init when roster does not render correctly
    """
    mock_hub.heist.init.run_remotes = hub.heist.init.run_remotes
    mock_hub.OPT.heist = {"roster": mock.sentinel.roster, "manager": "salt_master"}
    mock_hub.roster.init.read.return_value = {}

    await mock_hub.heist.init.run_remotes("grains", tempdir)

    mock_hub.roster.init.read.assert_called_with(None, roster_file="", roster_data=None)


@pytest.mark.parametrize(
    "addr",
    [
        ("127.0.0.1", True),
        ("::1", True),
        ("2001:0db8:85a3:0000:0000:8a2e:0370:7334", False),
        ("localhost", True),
        ("1.1.1.1", False),
        ("google.com", False),
    ],
)
def test_ip_is_loopback(addr, mock_hub):
    """
    Test for function ip_is_loopback
    when socket error raised, expected
    return is False
    """
    ret = heist.heist.init.ip_is_loopback(mock_hub, addr[0])
    assert ret == addr[1]


def test_ip_is_loopback_exception(mock_hub):
    """
    Test for function ip_is_loopback
    when address is not valid
    """
    assert not heist.heist.init.ip_is_loopback(mock_hub, "")
