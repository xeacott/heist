#!/usr/bin/python3
"""
    unit tests for the clustershell roster
"""
import socket

import asynctest
import pytest
from dict_tools.data import NamespaceDict

import tests.helpers


@pytest.mark.asyncio
async def test_read(mock_hub, hub):
    """
    test clustershell roster against port 22 localhost
    """
    mock_hub.roster.clustershell.read = hub.roster.clustershell.read
    # Setup
    target = socket.gethostname()
    addr = socket.gethostbyname(target)
    mock_hub.OPT.heist = NamespaceDict(target=target, ssh_scan_ports=[22])

    # Execute
    with asynctest.mock.patch(
        "asyncio.open_connection", side_effect=tests.helpers.async_read_write
    ):
        ret = await mock_hub.roster.clustershell.read()

    assert ret[addr]["host"] == addr
    assert ret[addr]["port"] == 22


@pytest.mark.asyncio
async def test_clustershell_non_ssh_port(mock_hub, hub):
    """
    test clustershell roster against port not running ssh
    """
    mock_hub.roster.clustershell.read = hub.roster.clustershell.read
    # Setup
    target = "127.0.0.1"
    mock_hub.OPT.heist = NamespaceDict(target=target, ssh_scan_ports=[22222])

    # Execute
    ret = await mock_hub.roster.clustershell.read()

    assert ret == {}
