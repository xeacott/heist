#!/usr/bin/python3
import tempfile
import textwrap
from typing import Tuple

import pytest
from dict_tools.data import NamespaceDict

import heist.roster.flat


FLAT_ROSTER = """#!yaml
host_nul_id:
host_str_id:
  id: unique_id
host_num_id:
  id: 1234
"""


@pytest.fixture
def flat_roster():
    """
    setup temporary roster directory and file
    """
    with tempfile.TemporaryDirectory(prefix="heist_flat_", suffix="_test") as temp_dir:
        with tempfile.NamedTemporaryFile(
            "w+", dir=temp_dir, suffix=".yml", delete=False
        ) as flat_roster:
            flat_roster.file.write(FLAT_ROSTER)
        yield temp_dir, flat_roster.name


@pytest.mark.asyncio
async def test_read(mock_hub, flat_roster: Tuple[str, str]):
    """
    test reading a yaml file with mock_hub
    """
    # Setup
    temp_dir, roster = flat_roster
    mock_hub.OPT = NamespaceDict(
        heist={"roster_dir": temp_dir, "renderer": "jinja|yaml"}
    )

    # Execute
    await heist.roster.flat.read(mock_hub)

    # Verify
    mock_hub.rend.init.parse.assert_called_once_with(fn=roster, pipe="jinja|yaml")


@pytest.mark.asyncio
async def test_read_rend_shebang(hub, flat_roster: Tuple[str, str]):
    """
    read yaml file with shebang at top of roster,
    and renderer is set to 'toml'
    """
    # Setup
    temp_dir, roster = flat_roster
    with open(roster, "w") as _f:
        _f.write(
            textwrap.dedent(
                """\
                 #!jinja|yaml
                 {% set user = 'heist' %}
                 localhost:
                   user: {{ user }}
                 """
            )
        )
    hub.OPT = NamespaceDict(heist={"roster_dir": temp_dir, "renderer": "toml"})

    # Execute
    result = await heist.roster.flat.read(hub)

    assert result["localhost"]["user"] == "heist"


@pytest.mark.asyncio
async def test_read_rend_no_shebang(hub, flat_roster: Tuple[str, str]):
    """
    read yaml file without shebang at top of roster,
    and renderer is set to 'jinja|yaml'
    """
    # Setup
    temp_dir, roster = flat_roster
    with open(roster, "w") as _f:
        _f.write(
            textwrap.dedent(
                """\
                 {% set user = 'heist' %}
                 localhost:
                   user: {{ user }}
                 """
            )
        )
    hub.OPT = NamespaceDict(heist={"roster_dir": temp_dir, "renderer": "jinja|yaml"})

    # Execute
    result = await heist.roster.flat.read(hub)

    assert result["localhost"]["user"] == "heist"


@pytest.mark.asyncio
async def test_roster_file(mock_hub, hub, flat_roster: Tuple[str, str]):
    """
    test when using roster_file in flat roster
    """
    mock_hub.roster.flat.read = hub.roster.flat.read
    # Setup
    temp_dir, roster = flat_roster
    mock_hub.OPT.heist = NamespaceDict(
        renderer="jinja|yaml", roster_file=roster, roster_dir=temp_dir
    )

    # Execute
    ret = await mock_hub.roster.flat.read()

    # Verify
    assert isinstance(ret, dict)
    mock_hub.rend.init.parse.assert_called_once_with(fn=roster, pipe="jinja|yaml")
