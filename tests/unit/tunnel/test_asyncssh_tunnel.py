#!/usr/bin/python3
import unittest.mock as mock

from dict_tools.data import NamespaceDict

import heist.tunnel.asyncssh as asyncssh_tunnel


def test__get_asyncssh_opt_target(mock_hub):
    """
    Test getting option from the target
    """
    mock_hub.OPT = {"heist": {"username", "opt"}}
    target = {"username": "target"}
    result = asyncssh_tunnel._get_asyncssh_opt(
        mock_hub, target=target, option="username", default="default"
    )
    assert result == "target"


def test__get_asyncssh_opt_config(mock_hub):
    """
    Test getting option from the config if target isn't available
    """
    mock_hub.OPT = NamespaceDict(heist={"username": "opt"})
    target = {}
    result = asyncssh_tunnel._get_asyncssh_opt(
        mock_hub, target=target, option="username", default="default"
    )
    assert result == "opt"


@mock.patch(
    "heist.tunnel.asyncssh._autodetect_asyncssh_opt",
    return_value="autodetect",
)
def test__get_asyncssh_opt_autodetect(mock_hub):
    """
    Test getting option from autodetect of target and config aren't available
    """
    mock_hub.OPT = NamespaceDict(heist={"roster_defaults": {}})
    target = {}
    result = asyncssh_tunnel._get_asyncssh_opt(
        mock_hub, target=target, option="username", default="default"
    )
    assert result == "autodetect"


def test_autodetect_asyncssh_opt(mock_hub):
    ...
